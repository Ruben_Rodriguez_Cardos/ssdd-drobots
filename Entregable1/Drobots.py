#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('drobots.ice')
import drobots


class RobotControllerI(drobots.RobotController):

	def __init__(self,bot,current=None):
		self.bot = bot

	def turn(self,current=None):
		print("Turn")

	def robotDestroyed(self,current=None):
		print("Robot Destroyed")

class PlayerI(drobots.Player):

	def makeController(self,bot,current=None):
		
		controllerServant = RobotControllerI(bot)
		proxyController=current.adapter.addWithUUID(controllerServant)
		controller = drobots.RobotControllerPrx.uncheckedCast(proxyController)

		return controller


	def win(self,current=None):
		print("You win")
		current.adapter.getCommunicator().shutdown()

	def lose(self,current=None):
		print("You loose")
		current.adapter.getCommunicator().shutdown()

	def gameAbort():
		print("Game Aborted")
		current.adapter.getCommunicator().shutdown()



class Drobots(Ice.Application):
    def run(self, argv):
        proxy = self.communicator().propertyToProxy("GameServer")
        game = drobots.GamePrx.checkedCast(proxy)

        if not game:
            raise RuntimeError('Invalid proxy')

        broker = self.communicator()#Recupero el broker
        adapter = broker.createObjectAdapter("GameAdapter")
        adapter.activate()

        servantPlayer = PlayerI()#Creo el sirviente
        proxyPlayer=adapter.addWithUUID(servantPlayer)#Pongo proxy al adaptador
        player = drobots.PlayerPrx.uncheckedCast(proxyPlayer)#Creo el objeto

        print("Mi Proxy: "+broker.proxyToString(proxyPlayer))

        try:
            game.login(player,str(argv[2]))
        except drobots.InvalidProxy:
        	print("Proxy Incorrecto")
        except drobots.InvalidName:
        	print("Nombre Incorrecto")
        except drobots.GameInProgress:
        	print("Juego en progreso")
        
        
        
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


sys.exit(Drobots().main(sys.argv))
