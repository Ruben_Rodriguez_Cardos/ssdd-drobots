#include <drobots.ice>

module drobotsUtil{
  interface RobotsFactory {
    ::drobots::RobotController* make(::drobots::Robot* robot);
    ::drobots::DetectorController* makeDetector();
  };

  exception AlreadyExists { string key; };
  exception NoSuchKey { string key; };

  dictionary<string, Object*> ObjectPrxDict;

  interface Container {
    void link(string key, Object* proxy) throws AlreadyExists;
    void unlink(string key) throws NoSuchKey;
    ObjectPrxDict list();
  };

  interface InterController extends ::drobots::RobotController{
    ::drobots::Point requestLocation();
    void shoot(::drobots::Point pos, int angle);
    void alertReceived(::drobots::Point pos, int enemies);

  };  

};