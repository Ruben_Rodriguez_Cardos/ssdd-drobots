#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I. --all drobotsUtil.ice')
import drobots
import drobotsUtil


class PlayerI(drobots.Player):

	def __init__(self,adapter):
		self.adapter=adapter
		self.return_value = 0
		self.n=1

	def makeController(self,bot,current=None):
		try:
			#Como accedo al objeto bien conocido?
			if self.n == 4:
				factoria="Factory3"
				self.n=1
			else:
				factoria="Factory"+str(self.n)
				self.n+=1

			print("Me conecto a "+factoria)	
			proxyFactory = Drobots.communicator().stringToProxy(factoria)
			factory = drobotsUtil.RobotsFactoryPrx.uncheckedCast(proxyFactory)
			if not factory:
				raise RuntimeError('Invalid proxy')
			
			robotController = factory.make(bot)
			print("Robot Controller Created!")
			sys.stdout.flush()
			return robotController

		except Exception as ex:
			print(ex)
			sys.stdout.flush()

	def makeDetectorController(self,current=None):
		#return DetectorController*
		print("Making a Detector Controller...")
		try:
			if self.n == 4:
				factoria="Factory3"
				self.n=1
			else:
				factoria="Factory"+str(self.n)
				self.n+=1

			print("Me conecto a "+factoria)	
			proxyFactory = Drobots.communicator().stringToProxy(factoria)
			factory = drobotsUtil.RobotsFactoryPrx.uncheckedCast(proxyFactory)
			if not factory:
				raise RuntimeError('Invalid proxy')
			
			detector = factory.makeDetector()
			print("Detector Controller Created!")
			sys.stdout.flush()
			return detector
		
		except Exception as ex:
			print(ex)
			sys.stdout.flush()

	def win(self,current=None):
		print("You win")
		sys.stdout.flush()
		current.adapter.getCommunicator().shutdown()
		return self.return_value

	def lose(self,current=None):
		print("You loose")
		sys.stdout.flush()
		current.adapter.getCommunicator().shutdown()
		self.return_value = -1
		return self.return_value

	def gameAbort(self,current=None):
		print("Game Aborted")
		sys.stdout.flush()
		current.adapter.getCommunicator().shutdown()



class Drobots(Ice.Application):
    def run(self, argv):
        proxy = self.communicator().propertyToProxy("GameServer")
        game = drobots.GamePrx.checkedCast(proxy)

        if not game:
            raise RuntimeError('Invalid proxy')

        print("Correct Proxy!")
        sys.stdout.flush()

        broker = self.communicator()#Recupero el broker
        adapter = broker.createObjectAdapter("GameAdapter")
        adapter.activate()

        servantPlayer = PlayerI(adapter)#Creo el sirviente
        proxyPlayer=adapter.addWithUUID(servantPlayer)#Pongo proxy al adaptador
        proxyPlayer=adapter.createDirectProxy(proxyPlayer.ice_getIdentity())
        player = drobots.PlayerPrx.uncheckedCast(proxyPlayer)#Creo el objeto

        print("Mi Proxy: "+broker.proxyToString(proxyPlayer))
        sys.stdout.flush()
        
        try:
            game.login(player,broker.getProperties().getProperty("Name"))
        except drobots.InvalidProxy:
        	print("Proxy Incorrecto")
        	sys.stdout.flush()
        except drobots.InvalidName:
        	print("Nombre Incorrecto")
        	sys.stdout.flush()
        except drobots.GameInProgress:
        	print("Juego en progreso")
        	sys.stdout.flush()
        
        
        
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


sys.exit(Drobots().main(sys.argv))
