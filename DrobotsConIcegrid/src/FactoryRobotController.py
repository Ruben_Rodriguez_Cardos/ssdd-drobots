#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I. --all drobotsUtil.ice')
import drobots
import drobotsUtil

import math
import random

class DetectorControllerI(drobots.DetectorController):

    def alert(self,pos,enemies,current=None):
        print("Detectados Robots")
        print(pos,enemies)
        #Invoco a metodo alertReceived de los robotController
        proxyContainer = FactoryRobotController.communicator().stringToProxy("ContainerProxys")
        container = drobotsUtil.ContainerPrx.checkedCast(proxyContainer)
        proxys=container.list().values()
        for proxy in proxys:
            ic=drobotsUtil.InterControllerPrx.checkedCast(proxy)
            ic.alertReceived(pos,enemies)



class RobotControllerAttackerI(drobotsUtil.InterController):
    
    def __init__(self,bot,current=None):
        self.turno=0
        self.bot=bot
        self.posicion=self.bot.location()
        self.candidatas=[]
        self.live = True
        self.studyCandidate = True


    def turn(self,current=None):
        try:
            self.turno+=1
            self.posicion=self.bot.location()

            #Recupero las posiciones
            posiciones=[]
            proxyContainer = FactoryRobotController.communicator().stringToProxy("ContainerProxys")
            container = drobotsUtil.ContainerPrx.checkedCast(proxyContainer)
            proxys=container.list().values()
            for proxy in proxys:
                aux=str(proxy)
                if not current.id.name in aux:
                    ic=drobotsUtil.InterControllerPrx.checkedCast(proxy)
                    posiciones.append(ic.requestLocation())

            more =True
            valid = True
            i = 50
            x=0
            y=0

            while more == True and i > 0:
                i-=1
                x=random.randint(0,399)
                y=random.randint(0,399)

                for pos in posiciones:
                    distancia=math.hypot(abs(x-pos.x),abs(y-pos.y))
                    if distancia < 40:
                        valid = False

                if valid == True:
                    more = False
                else:
                    more = True
        	
            #Calculo angulo y distancia al punto
            # Calculo el angulo
            dx = x-self.posicion.x
            dy = y-self.posicion.y
            # Calculo el angulo atan2 argumentos al reves
            angulo=math.atan2(float(dy),float(dx))#Arcotangente
            angulo=math.degrees(angulo)#Lo paso a grados
            #Calculo la distancia
            distancia=math.hypot(abs(x-self.posicion.x),abs(y-self.posicion.y)) 

            #Si existen candidatas las estudiamos, ademas no todos los turnos se estudiaran candidatas.
            for candidata in self.candidatas:
                for pos in posiciones:
                    if candidata.x == pos.x:
                        if candidata.y == candidata .y:
                            candidatas.remove(candidata)

            if len(self.candidatas) > 0 and self.studyCandidate == True:
                print("Existen posiciones candidatas a estudiar")
                sys.stdout.flush()
                pos = self.candidatas[0]
                #Calculo angulo y distancia al punto
                # Calculo el angulo
                dx = pos.x-self.posicion.x
                dy = pos.y-self.posicion.y
                # Calculo el angulo atan2 argumentos al reves
                angulo=math.atan2(float(dy),float(dx))#Arcotangente
                angulo=math.degrees(angulo)#Lo paso a grados
                #Calculo la distancia
                distancia=math.hypot(abs(x-self.posicion.x),abs(y-self.posicion.y))
                self.studyCandidate == False
                self.candidatas=[]

            self.bot.cannon(angulo,distancia)

            if self.studyCandidate == True:
                self.studyCandidate = False
            else:
                self.studyCandidate = True
        
        except Exception as ex:
			print("Excepcion: ",ex)
			sys.stdout.flush()

        except drobots.NoEnoughEnergy :
        	print("Ups, energia insuficiente....")
        	sys.stdout.flush()

    def robotDestroyed(self,current=None):
        try:
            if self.live == True:
            	print ("\nRobot Destruido\n")
            	sys.stdout.flush()
            	proxyContainer = FactoryRobotController.communicator().stringToProxy("ContainerProxys")
            	container = drobotsUtil.ContainerPrx.checkedCast(proxyContainer)
            	container.unlink(current.id.name)
            	self.live=False
        except Exception as ex:
            print("Excepcion: ",ex)
            sys.stdout.flush()

    def requestLocation(self,current=None):
    	return self.posicion

    def alertReceived(self,pos,enemies,current=None):
        print("Alerta Recibida")
        print(pos,enemies)
        #Añadir a lista candidatas
        self.candidatas.append(pos)

class RobotControllerDefenderI(drobotsUtil.InterController):
    
    def __init__(self,bot,current=None):
        self.turno=0
        self.bot=bot
        self.posicion=self.bot.location()
        self.energy = 100
        self.live = True

    def turn(self,current=None):
        self.energy=100
        """print ("\nEstoy en turn Defender\n")
        sys.stdout.flush()
        print("Soy "+current.id.name)
        sys.stdout.flush()
        print ("Turno: ",self.turno)
        sys.stdout.flush()"""
        self.turno+=1
        self.posicion=self.bot.location()
        #print("Posicion: ",self.posicion.x,self.posicion.y)
        #sys.stdout.flush()

        while self.energy > 10:
        	aux=self.bot.scan(random.randint(0,359),10)
        	#print("Localizados ",aux," recibidos")
        	#sys.stdout.flush()
        	self.energy-=10
    
    def robotDestroyed(self,current=None):
        try:
            if self.live == True:
            	print ("\nRobot Destruido\n")
            	sys.stdout.flush()
            	proxyContainer = FactoryRobotController.communicator().stringToProxy("ContainerProxys")
            	container = drobotsUtil.ContainerPrx.checkedCast(proxyContainer)
            	container.unlink(current.id.name)
            	self.live=False
        except Exception as ex:
            print("Excepcion: ",ex)
            sys.stdout.flush()

    def requestLocation(self,current=None):
    	return self.posicion

    def alertReceived(self,pos,enemies,current=None):
        print("Alerta Recibida")
        sys.stdout.flush()
        #print(pos,enemies)
        #Añadir a lista candidatas    


class RobotFactoryI(drobotsUtil.RobotsFactory): 
 	
    def __init__ (self,current=None):
        self.inv=0;

    def makeDetector(self,current=None):
        print("Making a Detector...")
        sys.stdout.flush()
        detector=DetectorControllerI()
        proxy=current.adapter.createDirectProxy(current.adapter.addWithUUID(detector).ice_getIdentity())
        print("Detector proxy: ", proxy)
        sys.stdout.flush()
        res=drobots.DetectorControllerPrx.uncheckedCast(proxy)
        return res

    def make(self,bot,current=None):
        print("Making the Controller...")
        sys.stdout.flush()
        
        if bot.ice_isA('::drobots::Defender'):
        	print("Making Defender...")
        	sys.stdout.flush()
        	robotcontroller=RobotControllerDefenderI(bot)

       	if bot.ice_isA('::drobots::Attacker'):
       		print("Making Defender...")
        	sys.stdout.flush()
        	robotcontroller=RobotControllerAttackerI(bot)
        
        print("add to adapter...")
        sys.stdout.flush()
        proxyRobotController = current.adapter.createDirectProxy(current.adapter.addWithUUID(robotcontroller).ice_getIdentity())
        print("RC proxy: ", proxyRobotController)
        sys.stdout.flush()
        

        robotcontroller1 = drobotsUtil.InterControllerPrx.uncheckedCast(proxyRobotController)
        #hago link al container
        proxyContainer = FactoryRobotController.communicator().stringToProxy("ContainerProxys")
        container = drobotsUtil.ContainerPrx.checkedCast(proxyContainer)
        container.link(proxyRobotController.ice_getIdentity().name, proxyRobotController)
        self.inv+=1

        return robotcontroller1


class FactoryRobotController(Ice.Application):
    
    def run(self, argv):

        broker = self.communicator()
        adapter = broker.createObjectAdapter("Adapter")
        servantRF=RobotFactoryI();#Creo el sirviente
        adapter.activate()#Lo Activo
        
        identity = broker.getProperties().getProperty("Name")
        proxyRF=adapter.add(servantRF, broker.stringToIdentity(identity))#Pongo proxy al adaptador
        
        print(proxyRF)
        sys.stdout.flush()

        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


sys.exit(FactoryRobotController().main(sys.argv))