#!/usr/bin/python -u
# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import Ice
Ice.loadSlice('-I. --all drobotsUtil.ice')
import drobotsUtil



class ContainerI(drobotsUtil.Container):
    def __init__(self):
        self.proxies = dict()

    def link(self, key, proxy, current=None):
        if key in self.proxies:
            raise drobotsUtil.AlreadyExists(key)

        print("link: {0} -> {1}".format(key, proxy))
        sys.stdout.flush()
        self.proxies[key] = proxy

    def unlink(self, key, current=None):
        if not key in self.proxies:
            raise drobotsUtil.NoSuchKey(key)

        print("unlink: {0}".format(key))
        sys.stdout.flush()
        del self.proxies[key]

    def list(self, current=None):
        return self.proxies


class ContainerProxys(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = ContainerI()

        adapter = broker.createObjectAdapter("ContainerAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("ContainerProxys"))

        print(proxy)

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


if __name__ == '__main__':
    sys.exit(ContainerProxys().main(sys.argv))
