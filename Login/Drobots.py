#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('drobots.ice')
import drobots


class PlayerI(drobots.Player):

	def getID(self,current=None):
		return "05708836Y"

	def win(self,current=None):
		print("You win")
		current.adapter.getCommunicator().shutdown()

	def lose(self,current=None):
		print("You loose")
		current.adapter.getCommunicator().shutdown()


class Drobots(Ice.Application):
    def run(self, argv):
        proxy = self.communicator().stringToProxy(argv[1])
        game = drobots.GamePrx.checkedCast(proxy)

        if not game:
            raise RuntimeError('Invalid proxy')

        broker = self.communicator()#Recupero el broker
        adapter = broker.createObjectAdapter("GameAdapter")
        adapter.activate()

        servantPlayer = PlayerI()#Creo el sirviente
        proxyPlayer=adapter.addWithUUID(servantPlayer)#Pongo proxy al adaptador
        player = drobots.PlayerPrx.uncheckedCast(proxyPlayer)#Creo el objeto

        print("Mi Proxy: "+broker.proxyToString(proxyPlayer))

        try:
        	#game.loginSimple(str(proxyPlayer),"Ruben")
            game.login(player,"Ruben")
        except drobots.InvalidProxy:
        	print("Proxy Incorrecto")
        except drobots.InvalidName:
        	print("Nombre Incorrecto")
        
        
        
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


sys.exit(Drobots().main(sys.argv))
